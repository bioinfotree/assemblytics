# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

log:
	mkdir -p $@

ref.fa:
	ln -sf $(REF) $@

all.del.pe.tab:
	zcat <$(DEL) >$@

all.ins.pe.tab:
	zcat <$(INS) >$@

	

.META: %.del.filter.bed %.del.filter2.bed %.ins.filter2.bed %.ins.filter.bed %.Assemblytics_structural_variants.bed %.ins.filter2.reverse.bed
	1	reference
	2	ref_start
	3	ref_stop
	4	ID
	5	size
	6	strand
	7	type
	8	ref_gap_size
	9	query_gap_size
	10	query_coordinates
	11	method

#CONTIG_ASSTICS_BED = $(addsuffix .contig.Assemblytics_structural_variants.bed,$(SAMPLES))
SCAFF_ASSTICS_BED = $(addsuffix .scaff.Assemblytics_structural_variants.bed,$(SAMPLES))
# this function install all the links at once
%.annot.gff3.gz %.scaff.ins.pe.tab %.scaff.del.pe.tab %.Assemblytics_structural_variants.bed %.scaff.fa:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf ../phase_1/$(SAMPLE).scaff.Assemblytics_structural_variants.bed $(SAMPLE).scaff.Assemblytics_structural_variants.bed ) \
		$(shell ln -sf ../phase_1/$(SAMPLE).scaff.fa $(SAMPLE).scaff.fa) \
		$(shell ln -sf ../phase_1/$(SAMPLE).scaff.del.tab  $(SAMPLE).scaff.del.pe.tab) \
		$(shell ln -sf ../phase_1/$(SAMPLE).scaff.ins.tab  $(SAMPLE).scaff.ins.pe.tab) \
		$(shell ln -sf $(call get,$(SAMPLE),GENE) $(SAMPLE).annot.gff3.gz) \
	)
	sleep 3

# $(shell ln -sf ../phase_1/$(SAMPLE).contig.Assemblytics_structural_variants.bed $(SAMPLE).contig.Assemblytics_structural_variants.bed ) \
# $(shell ln -sf ../phase_1/$(SAMPLE).contig.fa $(SAMPLE).contig.fa) \

SCAFF_DEL_BED = $(addsuffix .scaff.del.filter.bed,$(SAMPLES))
# only deletions
%.del.filter.bed: %.Assemblytics_structural_variants.bed
	bawk '!/^[\#+,$$]/ { if ( $$type ~ /$(DEL_FILTER)/ ) print $$0 }' $< >$@

SCAFF_INS_BED = $(addsuffix .scaff.ins.filter.bed,$(SAMPLES))
# only insertions
%.ins.filter.bed: %.Assemblytics_structural_variants.bed
	bawk '!/^[\#+,$$]/ { if ( $$type ~ /$(INS_FILTER)/ ) print $$0 }' $< >$@



# SEQUENCE

# extract from reference
SCAFF_DEL_SEQ = $(addsuffix .scaff.del.filter.seq,$(SAMPLES))
%.scaff.del.filter.seq: ref.fa %.scaff.del.filter.bed
	$(call load_modules); \
	bedtools getfasta -fi $< -bed $^2 -tab -fo stdout \
	| bsort \   * some deletions could be duplicated, remove them *
	| uniq >$@

# extract from assembly
SCAFF_INS_SEQ = $(addsuffix .scaff.ins.filter.seq,$(SAMPLES))
%.scaff.ins.filter.seq: %.scaff.fa %.scaff.ins.filter.bed
	$(call load_modules); \
	bedtools getfasta -fi $< \
	-bed <(select_columns 10 4 <$^2 \
	| bawk '{ split($$1,a,":"); ref=a[1]; strand=a[3]; split(a[2],b,"-"); start=b[1]; stop=b[2]; \
	print ref, start, stop, $$2, stop-start, strand}' \
	| bsort \   * can be duplicated *
	| uniq) -tab -fo $@





define merge_seq
	(
	for F in $1; do
		P=$${F%.scaff.*.filter.seq}
		bawk -v p="$$P" '{ print p"."$$1, $$2 }' $$F
	done
	) \
	| bsort \
	| tab2fasta 2 \
	| gzip -c9 >$2
endef

# collect all variants to unique FILE for annotation
%.del.filter.fasta.gz: $(SCAFF_DEL_SEQ)
	$(call merge_seq,$^,$@)

%.ins.filter.fasta.gz: $(SCAFF_INS_SEQ)
	$(call merge_seq,$^,$@)



# generate tab file to map sequence names 
# to assemblytics name and sample
%.del.filter.map: $(SCAFF_DEL_BED)
	(
	for F in $^; do
		P=$${F%.scaff.*.filter.bed}
		bawk -v p="$$P" '{ print $$4, p"."$$1":"$$2"-"$$3; }' $$F
	done
	) >$@

%.ins.filter.map: $(SCAFF_INS_BED)
	(
	for F in $^; do
		P=$${F%.scaff.*.filter.bed}
		bawk -v p="$$P" '{ split($$10,a,":"); print $$4, p"."a[1]":"a[2]; }' $$F
	done
	) >$@


# to visualiza structure of the variants
%.scaff.del.filter.struct: %.scaff.del.filter.fasta.gz
	zcat $< \
	| fasta2tab \
	| bawk '!/^[\#+,$$]/ { \
		slice=$$2; \
		gsub(/[ABCDGHKMRSTVWY]+/,"sequence ",slice); \
		gsub(/N{10,}/,"gap ",slice); \
		gsub(/N{1}/,"single_N ",slice); \
		 print $$1, slice; \
	}' $< >$@

%.scaff.ins.filter.struct: %.scaff.ins.filter.fasta.gz
	fasta2tab <$< \
	| bawk '!/^[\#+,$$]/ { \
		slice=$$2; \
		gsub(/[ABCDGHKMRSTVWY]+/,"sequence ",slice); \
		gsub(/N{10,}/,"gap ",slice); \
		gsub(/N{1}/,"single_N ",slice); \
		 print $$1, slice; \
	}' $< >$@




# DEMULTIPLEX REPEATS ANNOTATIONS
import te.demux.annot




# REMOVE >=80% N
define filter_2
	filter_1col 4 <(\
	fasta2tab <$2 \
	| bawk '{ split($$1,a,"."); \
	if ( a[1] ~ /$3/ ) print a[2] }') <$1 >$4
endef

# remove variants that contains >=80% N exploiting the filter
# implemented in the ltr_find/phase_1 analysis
SCAFF_DEL_BED2 = $(addsuffix .scaff.del.filter2.bed,$(SAMPLES))
%.scaff.del.filter2.bed: %.scaff.del.filter.bed $(DEL_REMOVED_N_DATASET)/phase_1/reference.fasta.cleaned
	$(call filter_2,$<,$^2,$*,$@)

SCAFF_INS_BED2 = $(addsuffix .scaff.ins.filter2.bed,$(SAMPLES))
%.scaff.ins.filter2.bed: %.scaff.ins.filter.bed $(INS_REMOVED_N_DATASET)/phase_1/reference.fasta.cleaned
	$(call filter_2,$<,$^2,$*,$@)

%.ins.filter2.reverse.bed: %.ins.filter2.bed
	bawk '{ split($$query_coordinates,a,":"); ref=a[1]; strand=a[3]; split(a[2],b,"-"); start=b[1]; stop=b[2]; \
	print ref, start, stop, $$ID, $$size, $$strand, $$type, $$query_gap_size, $$ref_gap_size, $$reference":"$$ref_start"-"$$ref_stop, $$method }' $< >$@
#####





# REPEATS
SCAFF_TE_CONSENSUS_DEL_BED = $(addsuffix .scaff.del.te.consensus.tab,$(SAMPLES))
%.scaff.del.te.consensus.tab: %.scaff.del.filter2.bed
	$(call load_modules); \
	bedtools intersect \
	-bed \   * write output as BED, not bam *
	-f $(MINIMUM_DEL_OVERLAP) \
	-wo \
	-a <(bawk '!/^[\#+,$$]/ { print $$0 }' $< | sortBed -i stdin) \   * remove header *
	-b <(zcat $(TE_GFF) | sortBed -i stdin) \
	| sortBed -i stdin >$@

SCAFF_TE_CONSENSUS_INS_BED = $(addsuffix .scaff.ins.te.consensus.tab,$(SAMPLES))
%.scaff.ins.te.consensus.tab: %.scaff.ins.filter2.bed
	# $(call load_modules); \
	# bedtools intersect \
	# -bed \   * write output as BED, not bam *
	# -f $(MINIMUM_DEL_OVERLAP) \
	# -wo \
	# -a <(bawk '!/^[\#+,$$]/ { print $$0 }' $< | sortBed -i stdin) \   * remove header *
	# -b <(zcat $(TE_GFF) | sortBed -i stdin) \
	# | sortBed -i stdin >$@


.META: all.del.pe.tab
	2	chr
	3	start
	4	end
	13	cabernet_franc
	21	gouais_blanc
	24	kishmish_vatkana
	40	PN40024
	45	rkatsiteli
	47	sangiovese
	50	sultanina
	55	traminer
	88	summary_annotation


# DELETIONS
cf_16.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$cabernet_franc >= 0.25 && $$cabernet_franc != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

gb_3.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$gouais_blanc >= 0.25 && $$gouais_blanc != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

kv_4.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$kishmish_vatkana >= 0.25 && $$kishmish_vatkana != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

pn.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$PN40024 >= 0.25 && $$PN40024 != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

rk_4.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$rkatsiteli >= 0.25 && $$rkatsiteli != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

sg_4.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$sangiovese >= 0.25 && $$sangiovese != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

su_1.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$sultanina >= 0.25 && $$sultanina != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

tr_3.scaff.del.pe.tab: all.del.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$traminer >= 0.25 && $$traminer != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@
###



.META: all.ins.pe.tab
	2	chr
	3	start
	4	end
	11	cabernet_franc
	19	gouais_blanc
	22	kishmish_vatkana
	38	PN40024
	43	rkatsiteli
	45	sangiovese
	48	sultanina
	53	traminer
	68	summary_annotation

# INSERTIONS
cf_16.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$cabernet_franc >= 0.2 && $$cabernet_franc != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

gb_3.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$gouais_blanc >= 0.2 && $$gouais_blanc != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

kv_4.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$kishmish_vatkana >= 0.2 && $$kishmish_vatkana != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

pn.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$PN40024 >= 0.2 && $$PN40024 != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

rk_4.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$rkatsiteli >= 0.2 && $$rkatsiteli != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

sg_4.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$sangiovese >= 0.2 && $$sangiovese != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

su_1.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$sultanina >= 0.2 && $$sultanina != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@

tr_3.scaff.ins.pe.tab: all.ins.pe.tab
	bawk '!/^[\#+,$$]/ { if ( $$traminer >= 0.2 && $$traminer != "na"  ) print $$chr, $$start, $$end, $$summary_annotation }' $< \
	| unhead >$@
###


SCAFF_PE_OVERLAP_DEL_TAB = $(addsuffix .scaff.del.te.pe.tab,$(SAMPLES))
%.scaff.del.te.pe.tab: %.scaff.del.filter2.bed %.scaff.del.pe.tab
	$(call load_modules); \
	bedtools intersect \
	-bed \   * write output as BED, not bam *
	-f $(MINIMUM_DEL_OVERLAP) \   * a file
	-wo \
	-a <(bawk '!/^[\#+,$$]/ { print $$0 }' $< | sortBed -i stdin) \   * remove header *
	-b <(sortBed -i $^2) \
	| sortBed -i stdin >$@

SCAFF_PE_OVERLAP_INS_TAB = $(addsuffix .scaff.ins.te.pe.tab,$(SAMPLES))
%.scaff.ins.te.pe.tab: %.scaff.ins.filter2.bed %.scaff.ins.pe.tab %.scaff.del.te.pe.tab
	$(call load_modules); \
	bedtools closest \
	-D ref \   * report distance of B in extra column, with respect to the reference genome. B features with a lower (start, stop) are upstream
	-t first \   * in case of features with same distance, report the first that occurred in the B file
	-a <(bawk '!/^[\#+,$$]/ { print $$0 }' $< | sortBed -i stdin) \   * remove header *
	-b <(sortBed -i $^2) \
	| sortBed -i stdin \
	| filter_1col -v 4 <(cut -f4 $^3) \   * remove var already assigned to deletions *
	| bawk 'function abs(v) {return v < 0 ? -v : v} { \
	if ( abs($$16) <= $(MAXIMUM_INS_DISTANCE) && $$12 != "." ) print $$0 \   * retain items in b that is not null and with less then certain distance from a *
	}'>$@


# GENES

.META: %.scaff.del.gene.tab %.scaff.ins.gene.tab
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	reference
	12	ref_start
	13	ref_stop
	14	ID
	15	size
	16	strand
	17	type
	18	ref_gap_size
	19	query_gap_size
	20	query_coordinates
	21	method
	22	overlapping_bases



SCAFF_GENE_DEL_TAB = $(addsuffix .scaff.del.gene.tab,$(SAMPLES))
%.scaff.del.gene.tab: %.scaff.del.filter2.bed
	$(call load_modules); \
	bedtools intersect \
	-bed \   * write output as BED, not bam *
	-wo \
	-a <(cat <$(GENE_GFF) | gff2bed | sortBed -i stdin) \
	-b <(sortBed -i $<) \
	| sortBed -i stdin >$@
#		-f $(MINIMUM_DEL_OVERLAP) \


SCAFF_GENE_INS_TAB = $(addsuffix .scaff.ins.gene.tab,$(SAMPLES))
%.scaff.ins.gene.tab: %.scaff.ins.filter2.reverse.bed %.annot.gff3.gz
	$(call load_modules); \
	if [ -e "$^2" ]; then \
		bedtools intersect \
		-bed \   * write output as BED, not bam *
		-wo \
		-a <(zcat $^2 | gff2bed | sortBed -i stdin) \
		-b <(sortBed -i $<) \
		| sortBed -i stdin >$@; \
	else
		touch $@
	fi
#		-f $(MINIMUM_DEL_OVERLAP) \



SCAFF_EXON_INS_TAB = $(addsuffix .scaff.ins.exon.tab,$(SAMPLES))
SCAFF_EXON_DEL_TAB = $(addsuffix .scaff.del.exon.tab,$(SAMPLES))
# select only exons that are covered by the variants for  >= $(MINIMUM_DEL_OVERLAP)
%.exon.tab: %.gene.tab
	bawk 'function abs(v) {return v < 0 ? -v : v} \
	{ if (  $$feature ~ "exon"  &&  $$overlapping_bases / abs( $$end - $$start ) >= $(MINIMUM_DEL_OVERLAP)  ) { print $$0; } }' $< >$@


# REPORTING

.META: del.report
	1	#sample
	2	deletion
	3	empty removed
	4	te consensus
	5	pe overlap
	6	gene

del.report: $(SCAFF_DEL_BED) $(SCAFF_DEL_BED2) $(SCAFF_TE_CONSENSUS_DEL_BED) $(SCAFF_PE_OVERLAP_DEL_TAB) $(SCAFF_GENE_DEL_TAB) $(SCAFF_EXON_DEL_TAB)
	# header from META
	cat >$@ <<EOF
	#sample	insertions	empty removed	te consensus	pe overlap	variants_overlapping_1bp_genes	gene	mrna	exon	variants_overlapping_>=$(MINIMUM_DEL_OVERLAP)_exon	exon
	EOF
	
	del=($(SCAFF_DEL_BED))
	del2=($(SCAFF_DEL_BED2))
	te_cons=($(SCAFF_TE_CONSENSUS_DEL_BED))
	pe_overlap=($(SCAFF_PE_OVERLAP_DEL_TAB))
	gene=($(SCAFF_GENE_DEL_TAB))
	exon=($(SCAFF_EXON_DEL_TAB))

	for i in $${!del[*]}; do
		_tmp=$${del[$$i]}
		s=$${_tmp%.del.bed}
		
		n_del=$$(cut -f1,2,3 $${del[$$i]} | bsort | uniq | wc -l)
		n_del2=$$(cut -f1,2,3 $${del2[$$i]} | bsort | uniq | wc -l)
		n_te_cons=$$(cut -f1,2,3 $${te_cons[$$i]} | bsort | uniq | wc -l)
		n_pe_overlap=$$(cut -f1,2,3 $${pe_overlap[$$i]} | bsort | uniq | wc -l)
		n_overlap_gene=$$(cut -f11,12,13 $${gene[$$i]} | bsort | uniq | wc -l)
		n_overlap_exon=$$(cut -f11,12,13 $${exon[$$i]} | bsort | uniq | wc -l)
		
		n_gene=0
		if grep -qc 'gene' <$${gene[$$i]}; then
			n_gene=$$(cut -f1-3,8 $${gene[$$i]} | bsort | uniq | grep -ic 'gene')
		fi
		
		n_mrna=0
		if grep -qc 'mRNA' <$${gene[$$i]}; then
			n_mrna=$$(cut -f1-3,8 $${gene[$$i]} | bsort | uniq | grep -ic 'mRNA')
		fi
		
		n_exon=0
		if grep -qc 'exon' <$${gene[$$i]}; then
			n_exon=$$(cut -f 1-3,8 $${gene[$$i]} | bsort | uniq | grep -ic 'exon')
		fi
		
		n_exon1=0
		if grep -qc 'exon' <$${exon[$$i]}; then
			n_exon1=$$(cut -f 1-3,8 $${exon[$$i]} | bsort | uniq | grep -ic 'exon')
		fi
	
		cat >>$@ <<EOF
		$$s	$$n_ins	$$n_ins2		$$n_pe_overlap	$$n_overlap_gene	$$n_gene	$$n_mrna	$$n_exon	$$n_overlap_exon	$$n_exon1
		EOF
	done



.META: ins.report
	1	#sample
	2	insertions
	3	empty removed
	4	te consensus
	5	pe overlap
	6	gene

ins.report: $(SCAFF_INS_BED) $(SCAFF_INS_BED2) $(SCAFF_PE_OVERLAP_INS_TAB) $(SCAFF_GENE_INS_TAB) $(SCAFF_EXON_INS_TAB) $(SCAFF_EXON_INS_TAB)
	# header from META
	cat >$@ <<EOF | bsort -V -k 1,1
	#sample	insertions	empty removed	te consensus	pe overlap	variants_overlapping_1bp_genes	gene	mrna	exon	variants_overlapping_>=$(MINIMUM_DEL_OVERLAP)_exon	exon
	EOF
	
	ins=($(SCAFF_INS_BED))
	ins2=($(SCAFF_INS_BED2))
	pe_overlap=($(SCAFF_PE_OVERLAP_INS_TAB))
	gene=($(SCAFF_GENE_INS_TAB))
	exon=($(SCAFF_EXON_INS_TAB))

	for i in $${!ins[*]}; do
		_tmp=$${ins[$$i]}
		s=$${_tmp%.ins.bed}
		
		n_ins=$$(cut -f10 $${ins[$$i]} | bsort | uniq | wc -l)
		n_ins2=$$(cut -f10 $${ins2[$$i]} | bsort | uniq | wc -l)
		n_pe_overlap=$$(cut -f10 $${pe_overlap[$$i]} | bsort | uniq | wc -l)
		n_overlap_gene=$$(cut -f11,12,13 $${gene[$$i]} | bsort | uniq | wc -l)
		n_overlap_exon=$$(cut -f11,12,13 $${exon[$$i]} | bsort | uniq | wc -l)
		
		n_gene=0
		if grep -qc 'gene' <$${gene[$$i]}; then
			n_gene=$$(cut -f4 $${gene[$$i]} | bsort | uniq | grep -ic 'gene')
		fi
		
		n_mrna=0
		if grep -qc 'mRNA' <$${gene[$$i]}; then
			n_mrna=$$(cut -f4 $${gene[$$i]} | bsort | uniq | grep -ic 'mRNA')
		fi
		
		n_exon=0
		if grep -qc 'exon' <$${gene[$$i]}; then
			n_exon=$$(cut -f 1-3,8 $${gene[$$i]} | bsort | uniq | grep -ic 'exon')
		fi
		
		n_exon1=0
		if grep -qc 'exon' <$${exon[$$i]}; then
			n_exon1=$$(cut -f 1-3,8 $${exon[$$i]} | bsort | uniq | grep -ic 'exon')
		fi
	
		cat >>$@ <<EOF
		$$s	$$n_ins	$$n_ins2		$$n_pe_overlap	$$n_overlap_gene	$$n_gene	$$n_mrna	$$n_exon	$$n_overlap_exon	$$n_exon1
		EOF
	done



.PHONY: test
test:
	@echo $(VAR)


ALL += 	$(SCAFF_EXON_INS_TAB) \
	$(SCAFF_EXON_DEL_TAB) \
	$(SCAFF_INS_SEQ) \
	$(SCAFF_DEL_SEQ) \
	$(SCAFF_DEL_BED2) \
	$(SCAFF_INS_BED2) \
	$(SCAFF_TE_CONSENSUS_DEL_BED) \
	$(SCAFF_PE_OVERLAP_DEL_TAB) \
	$(SCAFF_PE_OVERLAP_INS_TAB) \
	$(SCAFF_GENE_DEL_TAB) \
	$(SCAFF_GENE_INS_TAB) \
	ins.report \
	del.report

	# all.scaff.del.filter.fasta.gz \
	# all.scaff.ins.filter.fasta.gz \

INTERMEDIATE += 


CLEAN += 
