# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

MIN_REF_LEN ?= 0

# NUCMER ALIGNMENT EXAMPLE
file.A.fasta:
	wget -O $@ http://mummer.sourceforge.net/examples/data/B_anthracis_Mslice.fasta

file.B.fasta:
	wget -O $@ http://mummer.sourceforge.net/examples/data/B_anthracis_contigs.fasta

nucmer.delta: file.A.fasta file.B.fasta
	$(call load_modules); \
	nucmer -maxmatch -c 100 -p nucmer $< $^2

nucmer.coords: nucmer.delta
	$(call load_modules); \
	show-coords -r -c -l $< >$@

nucmer.snps: nucmer.delta
	$(call load_modules); \
	show-snps -C $< >$@

nucmer.tiling: nucmer.delta
	$(call load_modules); \
	show-tiling $< >$@

# ALIGNMENT VISUALIZATION
mapview_0.pdf: nucmer.coords
	$(call load_modules); \
	mapview -n 1 -f pdf -p mapview $<


# TEST GAGE SCRIPTS
contig.fa:
	zcat <$(CONTIG) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( length($$NF) >= $(MIN_REF_LEN) ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 >$@

scaffold.fa:
	zcat <$(SCAFFOLD) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( length($$NF) >= $(MIN_REF_LEN) ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 >$@

ref.fa:
	zcat <$(REF) \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( length($$NF) >= $(MIN_REF_LEN) ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 >$@

getCorrectnessStats.sh: 
	wget -q http://gage.cbcb.umd.edu/results/gage-validation.tar.gz;
	tar xzf gage-validation.tar.gz;

scaffold.fa.coords: getCorrectnessStats.sh ref.fa contig.fa scaffold.fa
	$(call load_modules); \
	sh getCorrectnessStats.sh $^2 $^3 $^4

.PHONY: test
test:
	@echo 


ALL += 

INTERMEDIATE += 

CLEAN += 