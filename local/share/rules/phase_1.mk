# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

MINIMUM_VARIANT_LENGTH ?= 50
MAXIMUM_VARIANT_LENGTH ?= 10000
UNIQUE_LENGTH_REQUIRED ?= 10000
# fraction of variant called by assemblitics
MINIMUM_DEL_OVERLAP ?= 0.5
# distance between of variant called by assemblitics and the pe breakpoint
MAXIMUM_INS_DISTANCE ?= 100

ref.fa:
	ln -sf $(REF) $@

all.del.tab:
	zcat <$(DEL) >$@

all.ins.tab:
	zcat <$(INS) >$@

# this function install all the links at once
CONTIG = $(addsuffix .contig.fa,$(SAMPLES))
SCAFF = $(addsuffix .scaff.fa,$(SAMPLES))
%.contig.fa %.scaff.fa:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell zcat <$(call get,$(SAMPLE),CTIG) >$(SAMPLE).contig.fa) \
		$(shell zcat <$(call get,$(SAMPLE),SCAFF) >$(SAMPLE).scaff.fa) \
	)
	sleep 3

CONTIG_DELTA = $(addsuffix .contig.delta,$(SAMPLES))
SCAFF_DELTA = $(addsuffix .scaff.delta,$(SAMPLES))
%.delta: ref.fa %.fa
	$(call load_modules); \
	nucmer -maxmatch -l 100 -c 500 $< $^2 -prefix $(basename $@)

CONTIG_DELTA_GZ = $(addsuffix .contig.delta.gz,$(SAMPLES))
SCAFF_DELTA_GZ = $(addsuffix .scaff.delta.gz,$(SAMPLES))
%.delta.gz: %.delta
	gzip -c9 <$< >$@

# ASSTICS = $(addsuffix .Assemblytics_results.zip,$(SAMPLES))

# pn.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/b5G1vwaVVrgYEeATq70t/pn.Assemblytics_results.zip

# tr_3.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/zf3fBtz4Dor1J2P9Doko/tr_3.Assemblytics_results.zip

# su_1.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/hSLA0hgWuUB1vwe2PIU7/su_1.Assemblytics_results.zip

# sg_4.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/ZXWpiMaNARXZSHEMI0UY/sg_4.Assemblytics_results.zip

# rv_3.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/PrEUszXJDXfAv7BLpuX4/rv_3.Assemblytics_results.zip

# cf_16.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/CQDxjUMbcQZ89g7QDHgj/cf_16.Assemblytics_results.zip

# gb_3.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/HXSFZUngvA2oA59yy4Xq/gb_3.Assemblytics_results.zip

# kv_4.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/1GVmq259dMku0tjGJ1m7/kv_4.Assemblytics_results.zip

# rk_4.Assemblytics_results.zip:
	# wget -O $@ http://qb.cshl.edu/assemblytics/user_data/CGl8JlhL3z86QP60ogUc/rk_4.Assemblytics_results.zip

CONTIG_ASSTICS_BED = $(addsuffix .contig.Assemblytics_structural_variants.bed,$(SAMPLES))
SCAFF_ASSTICS_BED = $(addsuffix .scaff.Assemblytics_structural_variants.bed,$(SAMPLES))

# Assemblytics delta output_prefix unique_length_required path_to_R_scripts minimum_variant_length maximum_variant_length
%.Assemblytics_structural_variants.bed: %.delta
	$(call load_modules); \
	Assemblytics $< $* $(UNIQUE_LENGTH_REQUIRED) $$PRJ_ROOT/local/bin $(MINIMUM_VARIANT_LENGTH) $(MAXIMUM_VARIANT_LENGTH)

# %.Assemblytics_structural_variants.bed: %.Assemblytics_results.zip
	# unzip -j -C -o $<

# %.del.tab: all.del.tab
	# bRscript <(cat <<'EOF'
	# library(data.table)
	# setwd("$(PWD)")
	# inf = fread("$<", data.table=F,stringsAsFactors=F)
	# var = inf[,c(2,3,4,which(colnames(inf)=="cabernet_franc"))]
	# var = var[var[,4] >= 0.25 & var[,4] != "na",]
	# write.table(var, file="$@", quote=F, append=F, sep="\t", row.names=F, col.names=F)
	# EOF)


.META: all.del.tab
	2	chr
	3	start
	4	end
	13	cabernet_franc
	21	gouais_blanc
	24	kishmish_vatkana
	40	PN40024
	45	rkatsiteli
	47	sangiovese
	50	sultanina
	55	traminer

.META: all.ins.tab
	2	chr
	3	start
	4	end
	11	cabernet_franc
	19	gouais_blanc
	22	kishmish_vatkana
	38	PN40024
	43	rkatsiteli
	45	sangiovese
	48	sultanina
	53	traminer



.META: %.del.tab %.ins.tab
	1	reference
	2	ref_start
	3	ref_stop
	4	ID
	5	size
	6	strand
	7	type
	8	ref_gap_size
	9	query_gap_size
	10	query_coordinates
	11	method

# select DELETIONS that are specific of a sample from the list of all variants in all samples
cf_16.contig.del.tab cf_16.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$cabernet_franc >= 0.25 && $$cabernet_franc != "na"  ) print $$chr, $$start, $$end, $$cabernet_franc }' $< \
	| unhead >$@

gb_3.contig.del.tab gb_3.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$gouais_blanc >= 0.25 && $$gouais_blanc != "na"  ) print $$chr, $$start, $$end, $$gouais_blanc }' $< \
	| unhead >$@

kv_4.contig.del.tab kv_4.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$kishmish_vatkana >= 0.25 && $$kishmish_vatkana != "na"  ) print $$chr, $$start, $$end, $$kishmish_vatkana }' $< \
	| unhead >$@

pn.contig.del.tab pn.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$PN40024 >= 0.25 && $$PN40024 != "na"  ) print $$chr, $$start, $$end, $$PN40024 }' $< \
	| unhead >$@

rk_4.contig.del.tab rk_4.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$rkatsiteli >= 0.25 && $$rkatsiteli != "na"  ) print $$chr, $$start, $$end, $$rkatsiteli }' $< \
	| unhead >$@

sg_4.contig.del.tab sg_4.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$sangiovese >= 0.25 && $$sangiovese != "na"  ) print $$chr, $$start, $$end, $$sangiovese }' $< \
	| unhead >$@

su_1.contig.del.tab su_1.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$sultanina >= 0.25 && $$sultanina != "na"  ) print $$chr, $$start, $$end, $$sultanina }' $< \
	| unhead >$@

tr_3.contig.del.tab tr_3.scaff.del.tab: all.del.tab
	bawk '!/^[\#+,$$]/ { if ( $$traminer >= 0.25 && $$traminer != "na"  ) print $$chr, $$start, $$end, $$traminer }' $< \
	| unhead >$@
###

# select INSERTIONS that are specific of a sample from the list of all variants in all samples
cf_16.contig.ins.tab cf_16.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$cabernet_franc >= 0.2 && $$cabernet_franc != "na"  ) print $$chr, $$start, $$end, $$cabernet_franc }' $< \
	| unhead >$@

gb_3.contig.ins.tab gb_3.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$gouais_blanc >= 0.2 && $$gouais_blanc != "na"  ) print $$chr, $$start, $$end, $$gouais_blanc }' $< \
	| unhead >$@

kv_4.contig.ins.tab kv_4.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$kishmish_vatkana >= 0.2 && $$kishmish_vatkana != "na"  ) print $$chr, $$start, $$end, $$kishmish_vatkana }' $< \
	| unhead >$@

pn.contig.ins.tab pn.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$PN40024 >= 0.2 && $$PN40024 != "na"  ) print $$chr, $$start, $$end, $$PN40024 }' $< \
	| unhead >$@

rk_4.contig.ins.tab rk_4.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$rkatsiteli >= 0.2 && $$rkatsiteli != "na"  ) print $$chr, $$start, $$end, $$rkatsiteli }' $< \
	| unhead >$@

sg_4.contig.ins.tab sg_4.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$sangiovese >= 0.2 && $$sangiovese != "na"  ) print $$chr, $$start, $$end, $$sangiovese }' $< \
	| unhead >$@

su_1.contig.ins.tab su_1.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$sultanina >= 0.2 && $$sultanina != "na"  ) print $$chr, $$start, $$end, $$sultanina }' $< \
	| unhead >$@

tr_3.contig.ins.tab tr_3.scaff.ins.tab: all.ins.tab
	bawk '!/^[\#+,$$]/ { if ( $$traminer >= 0.2 && $$traminer != "na"  ) print $$chr, $$start, $$end, $$traminer }' $< \
	| unhead >$@
###

.META: %.overlap.del.tab %.ins.del.tab
	1	reference
	2	ref_start
	3	ref_stop
	4	ID
	5	size
	6	strand
	7	type
	8	ref_gap_size
	9	query_gap_size
	10	query_coordinates
	11	method
	12	reference2
	13	ref_start2
	14	ref_stop2
	15	haplotype
	16	overlap_length/distance

SCAFF_OVERLAP_DEL = $(addsuffix .scaff.overlap.del.tab,$(SAMPLES))
CONTIG_OVERLAP_DEL = $(addsuffix .contig.overlap.del.tab,$(SAMPLES))
CONTIG_DEL = $(addsuffix .contig.del.tab,$(SAMPLES))
%.overlap.del.tab: %.Assemblytics_structural_variants.bed %.del.tab
	$(call load_modules); \
	bedtools intersect \
	-bed \   * write output as BED, not bam *
	-f $(MINIMUM_DEL_OVERLAP) \   * a file
	-wo \
	-a <(bawk '!/^[\#+,$$]/ { print $$0 }' $< | sortBed -i stdin) \   * remove header *
	-b <(sortBed -i $^2) \
	| sortBed -i stdin >$@

SCAFF_OVERLAP_INS = $(addsuffix .scaff.overlap.ins.tab,$(SAMPLES))
CONTIG_OVERLAP_INS = $(addsuffix .contig.overlap.ins.tab,$(SAMPLES))
CONTIG_INS = $(addsuffix .contig.ins.tab,$(SAMPLES))
%.overlap.ins.tab: %.Assemblytics_structural_variants.bed %.ins.tab %.overlap.del.tab
	$(call load_modules); \
	bedtools closest \
	-D ref \   * report distance of B in extra column, with respect to the reference genome. B features with a lower (start, stop) are upstream
	-t first \   * in case of features with same distance, report the first that occurred in the B file
	-a <(bawk '!/^[\#+,$$]/ { print $$0 }' $< | sortBed -i stdin) \   * remove header *
	-b <(sortBed -i $^2) \
	| sortBed -i stdin \
	| filter_1col -v 4 <(cut -f4 $^3) \   * remove var already assigned to deletions *
	| bawk 'function abs(v) {return v < 0 ? -v : v} { \
	if ( abs($$16) <= $(MAXIMUM_INS_DISTANCE) && $$12 != "." ) print $$0 \   * retain items in b that is not null and with less then certain distance from a *
	}'>$@


# # for testing
# %.contig.only.overlap.del.tab:	%.contig.overlap.del.tab %.scaff.overlap.del.tab
	# # number of assemblytics variants in contigs that have:
	# #	same start in scaffolds
	# #	but different end
	# comm_same_start=$$(filter_2col --both-orders 1 2 <(filter_2col -v --both-orders 2 3 <(cut -f 2,3 $^2 | bsort) <$< | cut -f 1,2) <$^2 | wc -l)
	# # get number of marching variants (from pe mapping) found only in contigs
	# uniq_from_pe=$$(filter_2col -v --both-orders 13 14 <(cut -f 13,14 $^2 | bsort) <$< | wc -l)
	# # get number of assemblytics variants found only in contigs comparing chr and start
	# uniq_chr_start=$$(filter_2col -v --both-orders 1 2 <(cut -f 1,2 $^2 | bsort) <$< | wc -l)
	# # get number of assemblytics variants found only in contigs comparing start and end
	# uniq_start_end=$$(filter_2col -v --both-orders 2 3 <(cut -f 2,3 $^2 | bsort) <$< | wc -l)
	# echo "$$uniq_start_end" "$$uniq_chr_start" "$$comm_same_start" "$$uniq_from_pe"

	
	


.META: overlap.report
	1	#sample
	2	del from pe mapping
	3	del found in contigs
	4	del uniq to contig 
	5	Deletion
	6	Insertion
	7	Repeat_contraction
	8	Repeat_expansion
	9	Tandem_contraction
	10	Tandem_expansion
	11	del found in scaffolds
	12	Deletion
	13	Insertion
	14	Repeat_contraction
	15	Repeat_expansion
	16	Tandem_contraction
	17	Tandem_expansion
	18	ins from pe mapping
	19	ins found in contigs
	20	ins uniq to contig 
	21	Deletion
	22	Insertion
	23	Repeat_contraction
	24	Repeat_expansion
	25	Tandem_contraction
	26	Tandem_expansion
	27	ins found in scaffolds
	28	Deletion
	29	Insertion
	30	Repeat_contraction
	31	Repeat_expansion
	32	Tandem_contraction
	33	Tandem_expansion



overlap.report: $(CONTIG_DEL) $(SCAFF_OVERLAP_DEL) $(CONTIG_OVERLAP_DEL) $(CONTIG_INS) $(SCAFF_OVERLAP_INS) $(CONTIG_OVERLAP_INS)
	# header from META
	cat >$@ <<EOF
	$$(bawk -M $@ | cut -f2 | transpose)
	EOF

	del=($(CONTIG_DEL))
	dsf=($(SCAFF_OVERLAP_DEL))
	dct=($(CONTIG_OVERLAP_DEL))
	
	ins=($(CONTIG_INS))
	isf=($(SCAFF_OVERLAP_INS))
	ict=($(CONTIG_OVERLAP_INS))
	
	# generate file used in transaltion of types
	type_list=$$(cat <<'EOF'
	Deletion
	Insertion
	Repeat_contraction
	Repeat_expansion
	Tandem_contraction
	Tandem_expansion
	EOF
	)
	
	for i in $${!dsf[*]}; do
		_tmp=$${del[$$i]}
		s=$${_tmp%.contig.del.tab}
		
		##### INSERTIONS
		
		# total assemblytics variants in scaffolds
		n_dsf=$$(cut -f 12,13,14 $${dsf[$$i]} | bsort | uniq | wc -l)
		# total assemblytics variants contigs
		n_dct=$$(cut -f 12,13,14 $${dct[$$i]} | bsort | uniq | wc -l)
		
		# get category of variants from assemblitics in scaffolds
		type_dsf=$$(\
		echo "$$type_list" \
		| translate -v <(cut -f7 $${dsf[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1) 1\
		| transpose)
	
		# get category of variants from assemblitics in contigs
		type_dct=$$(\
		echo "$$type_list" \
		| translate -v <(cut -f7 $${dct[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1) 1\
		| transpose)
		
		# total number of variants from pe mapping
		n_del=$$(cat $${del[$$i]} | wc -l)
	
		# I calculated the number of variants shared between assemblitics scaffold and pe mapping
		# I calculated the number of variants shared between assemblitics contigs and pe mapping 
		# Here I get number of marching variants found only in contigs but not in scaffolds
		n_del_uniq_to_ct=$$(filter_2col -v --both-orders 13 14 <(cut -f 13,14 $${dsf[$$i]} | bsort) <$${dct[$$i]} | wc -l)

		##### DELETIONS
	  
		n_isf=$$(cut -f 12,13,14 $${isf[$$i]} | bsort | uniq | wc -l)
		n_ict=$$(cut -f 12,13,14 $${ict[$$i]} | bsort | uniq | wc -l)
	
		type_isf=$$(\
		echo "$$type_list" \
		| translate -v <(cut -f7 $${isf[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1) 1 \
		| transpose)
		
		type_ict=$$(\
		echo "$$type_list" \	
		| translate -v <(cut -f7 $${ict[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1) 1\
		| transpose)
	
		n_ins=$$(cat $${ins[$$i]} | wc -l)
		
		n_ins_uniq_to_ct=$$(filter_2col -v --both-orders 13 14 <(cut -f 13,14 $${isf[$$i]} | bsort) <$${ict[$$i]} | wc -l)
		
		##### WRITE TO FILE

		cat >>$@ <<EOF
		$$s	$$n_del	$$n_dct	$$n_del_uniq_to_ct	$$type_dct	$$n_dsf	$$type_dsf	$$n_ins	$$n_ict	$$n_ins_uniq_to_ct	$$type_ict	$$n_isf	$$type_isf
		EOF
	done


NOT_OVERLAPPING_ASSTICS_BED = $(addsuffix .not.overlap.tab,$(SAMPLES))
%.not.overlap.tab: %.scaff.overlap.ins.tab %.scaff.overlap.del.tab  %.scaff.Assemblytics_structural_variants.bed  %.contig.overlap.ins.tab %.contig.overlap.del.tab %.contig.Assemblytics_structural_variants.bed
	unhead <$^3 | filter_1col -v 4 <(cat <(cut -f4 <$<) <(cut -f4 $^2) | bsort | uniq) \
	| cat - <(unhead <$^6 | filter_1col -v 4 <(cat <(cut -f4 <$^4) <(cut -f4 $^5) | bsort | uniq)) \
	| select_columns $$(seq 3 11) 1 2 \
	| bsort -k 10,10 -k 11,11n \
	| uniq --skip-fields 9 \   * unique calculated comparing chr and start *
	| select_columns 10 11 $$(seq 1 9) >$@


.META: not.overlap.report
	1	#sample
	2	total assemblytics contig
	3	non pe in contigs
	4	total assemblytics scaffolds
	5	non pe in scaffold
	6	uniq assemblytics
	7	Deletion
	8	Insertion
	9	Repeat_contraction
	10	Repeat_expansion
	11	Tandem_contraction
	12	Tandem_expansion



not.overlap.report: $(SCAFF_OVERLAP_INS) $(SCAFF_OVERLAP_DEL) $(SCAFF_ASSTICS_BED) $(CONTIG_OVERLAP_INS) $(CONTIG_OVERLAP_DEL) $(CONTIG_ASSTICS_BED) $(NOT_OVERLAPPING_ASSTICS_BED)
	# header from META
	cat >$@ <<EOF
	$$(bawk -M $@ | cut -f2 | transpose)
	EOF

	# generate file used in transaltion of types
	type_list=$$(cat <<'EOF'
	Deletion
	Insertion
	Repeat_contraction
	Repeat_expansion
	Tandem_contraction
	Tandem_expansion
	EOF
	)
	
	isf=($(SCAFF_OVERLAP_INS))
	dsf=($(SCAFF_OVERLAP_DEL))
	asf=($(SCAFF_ASSTICS_BED))
	
	ict=($(CONTIG_OVERLAP_INS))
	dct=($(CONTIG_OVERLAP_DEL))
	act=($(CONTIG_ASSTICS_BED))

	nota=($(NOT_OVERLAPPING_ASSTICS_BED))
	
	for i in $${!dsf[*]}; do
		_tmp=$${dsf[$$i]}
		s=$${_tmp%%.scaff.overlap.del.tab}
	
		tot_assemblytics_scaff=$$(unhead <$${asf[$$i]} | wc -l)
		tot_assemblytics_contig=$$(unhead <$${act[$$i]} | wc -l)
		not_marching_in_scaff=$$(unhead <$${asf[$$i]} | filter_1col -v 4 <(cat <(cut -f4 <$${isf[$$i]}) <(cut -f4 $${dsf[$$i]}) | bsort | uniq) | wc -l)
		not_marching_in_contig=$$(unhead <$${act[$$i]} | filter_1col -v 4 <(cat <(cut -f4 <$${ict[$$i]}) <(cut -f4 $${dct[$$i]}) | bsort | uniq) | wc -l)
		not_matching_total=$$(cat $${nota[$$i]} | wc -l)
	
		type=$$(\
			echo "$$type_list" \
			| translate -v <(cut -f7 $${nota[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1) 1 \
			| transpose)
	
		cat >>$@ <<EOF
		$$s	$$tot_assemblytics_contig	$$not_marching_in_contig	$$tot_assemblytics_scaff	$$not_marching_in_scaff	$$not_matching_total	$$type
		EOF
	done
	


.PHONY: test
test:
	@echo $(VAR)


ALL += 	$(CONTIG_ASSTICS_BED) \
	$(SCAFF_ASSTICS_BED) \
	$(SCAFF_OVERLAP_DEL) \
	$(CONTIG_OVERLAP_DEL) \
	$(SCAFF_OVERLAP_INS) \
	$(CONTIG_OVERLAP_INS) \
	overlap.report \
	not.overlap.report
	# $(CONTIG_DELTA) \
	# $(SCAFF_DELTA) \
	# $(CONTIG_DELTA_GZ) \
	# $(SCAFF_DELTA_GZ) \

	


INTERMEDIATE += 


CLEAN += $(wildcard *.Assemblytics.*) \
	$(wildcard *.summary) \
	$(wildcard *.index) \
	$(wildcard *.bed) \
	$(wildcard *.csv) \
	$(wildcard *.genome) \
	$(wildcard *.txt) \
	$(wildcard *.coords.tab)