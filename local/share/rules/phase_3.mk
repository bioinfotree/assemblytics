# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

log:
	mkdir -p $@


.META: %.pn.fstats %.self.fstats %.rnaseq.fstats
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	duplicates
	4	mapped (98.79%:-nan%)
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired (97.16%:-nan%)
	9	with itself and mate mapped
	10	singletons (0.83%:-nan%)
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)



# this function install all the links at once
FINAL_SUMMARY = $(addsuffix .final.summary,$(SAMPLES))
RNASEQ_COV = $(addsuffix .scaff.total.gene.rnaseq.cov,$(SAMPLES))
ALN_PN = $(addsuffix .pn.merged.nodup.bam,$(SAMPLES))
ALN = $(addsuffix .self.merged.nodup.bam,$(SAMPLES))
FSTATS = $(addsuffix .self.fastats,$(SAMPLES))
FSTATS_PN = $(addsuffix .pn.fastats,$(SAMPLES))
FSTATS_RNSASEQ = $(addsuffix .rnaseq.fstats,$(SAMPLES))
%.final.summary %.scaff.total.gene.rnaseq.cov %.self.merged.nodup.bam %.pn.merged.nodup.bam %.scaff.del.gene.tab %.scaff.ins.gene.tab %.pn.fstats %.self.fstats %.rnaseq.fstats:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),SUMMARY) $(SAMPLE).final.summary) \
		$(shell ln -sf $(call get,$(SAMPLE),RNASEQ_COV) $(SAMPLE).scaff.total.gene.rnaseq.cov) \
		$(shell ln -sf $(call get,$(SAMPLE),ALN_PN) $(SAMPLE).pn.merged.nodup.bam) \
		$(shell ln -sf $(call get,$(SAMPLE),ALN) $(SAMPLE).self.merged.nodup.bam) \
		$(shell ln -sf ../phase_2/$(SAMPLE).scaff.del.exon.tab $(SAMPLE).scaff.del.gene.tab) \
		$(shell ln -sf ../phase_2/$(SAMPLE).scaff.ins.exon.tab $(SAMPLE).scaff.ins.gene.tab) \
		$(shell ln -sf $(call get,$(SAMPLE),ALN_FSTATS) $(SAMPLE).self.fstats) \
		$(shell ln -sf $(call get,$(SAMPLE),ALN_PN_FSTATS) $(SAMPLE).pn.fstats) \
		$(shell ln -sf $(call get,$(SAMPLE),RNASEQ_FSTATS) $(SAMPLE).rnaseq.fstats) \
	) \
	sleep 3

CHAIN = $(addsuffix .chain,$(SAMPLES))
%.chain: %.final.summary
	$(call load_modules); \
	allpaths2bed -c <$< >$@




# INSERTIONS

# convert coordinates of scaffold into corresponding contigs.
# the file converted contains "!" instead of <TAB> as separator
# NB: some genes will be lost. Lost genes are saved into %.contig.ins.gene.failed
TMP_CONTIG_INS_GENE_TAB = $(addsuffix .contig.ins.gene.tab.tmp,$(SAMPLES))
%.contig.ins.gene.tab.tmp: %.chain %.scaff.ins.gene.tab
	$(call load_modules); \
	SEP="!"; \
	CrossMap.py bed $< \
	<(awk -v sep="$$SEP" -F'\t' '/^scaff/ { \
	# from 1-based [oneStart, oneEnd] to [oneStart-1, oneEnd) \
	printf "%s\t%s\t%s\t", $$1, $$2, $$3; \
	for ( i=4; i<NF; i++ ) { \
		printf "%s%s", $$i, sep; } \
		printf "%s\n", $$NF; }' $^2) >$@

# NB: some genes or gene elements can FAIL! the conversion
# several genes are SPLITTED over multiple contigs
# probably because they spans several genes
CONTIG_INS_GENE_FAILED = $(addsuffix .contig.ins.gene.failed.splitted,$(SAMPLES))
%.contig.ins.gene.failed.splitted: %.contig.ins.gene.tab.tmp
	SPLITED=$$(cut -f5 $< | error_ignore -- grep -c 'split')
	UNSPLITTED=$$(cut -f5 $< | error_ignore -- grep -c '\->')
	FAILED=$$(cut -f5 $< | error_ignore -- grep -c 'Fail')
	cat >$@ <<EOF
	splitted	unsplitted	failed
	$$SPLITED	$$UNSPLITTED	$$FAILED
	EOF


.META: %.contig.ins.gene.tab
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description


# convert separator from "!" to <TAB>
CONTIG_INS_GENE_TAB = $(addsuffix .contig.ins.gene.tab,$(SAMPLES))
%.contig.ins.gene.tab: %.contig.ins.gene.tab.tmp
	SEP="!";
	cut -f 6-9 <$< \
	| sed -e '/^$$/d' -e 's/'"$$SEP"'/\t/g' \
	| cut -f 1-10 \
	| sort -k1,1 -k2,2n >$@   * need to invoke memory-efficient algorithm with bedtools coverage *



######## calculate coverage of genes mapped to scaffolds by read of rnaseq
######## mapped on scaffolds.
######## Then convert scaffold coordinates to contig coordinates
.META: %.scaff.rnaseq.total.gene.cov %.contig.ins.gene.rnaseq.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered

# select only genes aready selected in %.scaff.ins.gene.tab
%.scaff.ins.gene.rnaseq.cov: %.scaff.ins.gene.tab %.scaff.total.gene.rnaseq.cov
	cut -f 1-10 <$< \   * 10 fields are needed to guarantie uniqness of dictionary *
	| sed -e 's/\t/!/g' \
	| translate -a <(sed -e 's/\t/!/g' -e 's/!/\t/10g' <$^2) 1 \
	| sed -e 's/!/\t/g' >$@
	


%.contig.ins.gene.rnaseq.cov.tmp: %.chain %.scaff.ins.gene.rnaseq.cov
	$(call load_modules); \
	SEP="!"; \
	CrossMap.py bed $< \
	<(awk -v sep="$$SEP" -F'\t' '/^scaff/ { \
	# from 1-based [oneStart, oneEnd] to [oneStart-1, oneEnd) \
	printf "%s\t%s\t%s\t", $$1, $$2, $$3; \
	for ( i=4; i<NF; i++ ) { \
		printf "%s%s", $$i, sep; } \
		printf "%s\n", $$NF; }' $^2) >$@

CONTIG_INS_GENE_RNASEQ_FAILED = $(addsuffix .contig.ins.gene.rnaseq.cov.failed,$(SAMPLES))
%.contig.ins.gene.rnaseq.cov.failed: %.contig.ins.gene.rnaseq.cov.tmp
	SPLITED=$$(cut -f5 $< | grep -c 'split')
	UNSPLITTED=$$(cut -f5 $< | grep -c '\->')
	FAILED=$$(cut -f5 $< | error_ignore -- grep -c 'Fail')
	cat >$@ <<EOF
	splitted	unsplitted	failed
	$$SPLITED	$$UNSPLITTED	$$FAILED
	EOF


# convert separator from "!" to <TAB>
CONTIG_INS_GENE_RNASEQ_COV = $(addsuffix .contig.ins.gene.rnaseq.cov,$(SAMPLES))
.PRECIOUS: %.contig.ins.gene.rnaseq.cov
%.contig.ins.gene.rnaseq.cov: %.contig.ins.gene.rnaseq.cov.tmp
	SEP="!";
	cut -f 6-9 <$< \
	| sed -e '/^$$/d' -e 's/'"$$SEP"'/\t/g' \
	| sort -k1,1 -k2,2n >$@   * need to invoke memory-efficient algorithm with bedtools coverage *

########





######## calculate coverage of genes mapped to contigs by read of the sample itself
######## mapped on contigs
.META: %.contig.ins.gene.self.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered


CONTIG_INS_GENE_SELF_COV = $(addsuffix .contig.ins.gene.self.cov,$(SAMPLES))
.PRECIOUS: %.contig.ins.gene.self.cov
%.contig.ins.gene.self.cov: %.self.merged.nodup.bam %.contig.ins.gene.tab
	$(call load_modules); \
	bedtools coverage \
	-abam $< \
	-b $^2 \
	>$@

########





######## calculate coverage of genes mapped to contigs by read of pn
######## mapped on contigs
.META: %.contig.ins.gene.pn.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered

CONTIG_INS_GENE_PN_COV = $(addsuffix .contig.ins.gene.pn.cov,$(SAMPLES))
.PRECIOUS: %.contig.ins.gene.pn.cov
%.contig.ins.gene.pn.cov: %.pn.merged.nodup.bam %.contig.ins.gene.tab
	$(call load_modules); \
	bedtools coverage \
	-abam $< \
	-b $^2 \
	>$@

########


# normalize reads number with "Transcripts Per Kilobase Million" methods
# explanation of the method can be found here: http://www.rna-seqblog.com/rpkm-rpkm-and-tpm-clearly-explained/
# The original code of R code of countToTpm and countToFpkm function is here: https://haroldpimentel.wordpress.com/2014/05/08/what-the-rpkm-a-review-rna-seq-expression-units/
CONTIG_INS_EXON_LOG2RATIO = $(addsuffix .contig.ins.exon.log2ratio,$(SAMPLES))
%.contig.ins.exon.log2ratio: %.contig.ins.gene.self.cov %.self.fstats %.contig.ins.gene.rnaseq.cov %.rnaseq.fstats %.contig.ins.gene.pn.cov %.pn.fstats  
	$(call load_modules); \
	cat <<'EOF' | bRscript -
	
	options(width=10000)
	setwd("$(PWD)")
	
	# standard implementation
	countToTpm = function(counts, mapped, effLen) {
		rpk = counts / effLen
		denom = mapped / 1e6
		return(rpk / denom)
	}
	
	countToFpkm = function(counts, mapped, effLen) {
		return( exp( log(counts) + log(1e9) - log(effLen) - log(mapped) ) )
	}
	
	# define header
	fh = c("reference", "start", "end", "name", "score", "strand",
	"attribute","feature","frame","description")
	
	# self data
	vh = c("self_overlapping_reads","self_covered_positions","self_feature_length","self_feature_covered")
	self_data = bread(file="$<", header=F)
	colnames(self_data) = c(fh,vh)

	self_fstat = bread(file="$^2", header=F, check.names=T)
	self_mapped = as.integer(self_fstat[1,4])
	
	self_counts = as.integer(self_data[,11])
	self_len = self_data[,13]/1000
	self_data$$"self_rpkm" = with(self_data, countToFpkm(self_counts, self_mapped, self_len))

	# rnaseq data
	vh = c("rnaseq_overlapping_reads","rnaseq_covered_positions","rnaseq_feature_length","rnaseq_feature_covered")
	rnaseq_data = bread(file="$^3", header=F)
	colnames(rnaseq_data) = c(fh,vh)
	
	rnaseq_fstat = bread(file="$^4", header=F, check.names=T)
	rnaseq_mapped = as.integer(rnaseq_fstat[1,4])
	
	rnaseq_counts = as.integer(rnaseq_data[,11])
	rnaseq_len = rnaseq_data[,13]/1000
	rnaseq_data$$"rnaseq_rpkm" = with(rnaseq_data, countToFpkm(rnaseq_counts, rnaseq_mapped, rnaseq_len))
	
	# pn data
	vh = c("pn_overlapping_reads","pn_covered_positions","pn_feature_length","pn_feature_covered")
	pn_data = bread(file="$^5", header=F)
	colnames(pn_data) = c(fh,vh)
	
	pn_fstat = bread(file="$^6", header=F, check.names=T)
	pn_mapped = as.integer(pn_fstat[1,4])
	
	pn_counts = as.integer(pn_data[,11])
	pn_len = pn_data[,13]/1000
	pn_data$$"pn_rpkm" = with(pn_data, countToFpkm(pn_counts, pn_mapped, pn_len))
	
	# join everything
	merged_data = merge(merge(self_data, pn_data, by=fh, sort=F), rnaseq_data, by=fh, sort=F)
	
	# calculate log 2 ratio
	merged_data$$"self-pn_l2r" = with(merged_data, log2( (merged_data$$"self_rpkm" + 0.1) / (merged_data$$"pn_rpkm" + 0.1) ))
	merged_data$$"rnaseq-pn_l2r" = with(merged_data, log2( (merged_data$$"rnaseq_rpkm" + 0.1) / (merged_data$$"pn_rpkm" + 0.1) ))
	
	# select exons
	exondata = merged_data[ which(merged_data[,8] == 'exon'), ]
	
	# reorder columns
	exondata = unique( 
					exondata[ c(fh,  "self_feature_length",
								"self_covered_positions", "rnaseq_covered_positions", "pn_covered_positions",
								"self_overlapping_reads", "rnaseq_overlapping_reads", "pn_overlapping_reads",
								"self_feature_covered", "rnaseq_feature_covered", "pn_feature_covered",
								"self_rpkm", "rnaseq_rpkm", "pn_rpkm",
								"self-pn_l2r", "rnaseq-pn_l2r" ) ]
					)
	
	colnames(exondata)[which(names(exondata) == "self_feature_length")] = "feature_length"
	
	# print to file
	bwrite(exondata, file="$@", row.names=F)
	EOF




.META: %.chr.del.exon.novel
	1	reference
	2	start
	3	end
	4	name
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	feature_length
	12	self_covered_positions
	13	rnaseq_covered_positions
	14	pn_covered_positions
	15	self_overlapping_reads
	16	rnaseq_overlapping_reads
	17	pn_overlapping_reads
	18	self_feature_covered
	19	rnaseq_feature_covered
	20	pn_feature_covered
	21	self_rpkm
	22	rnaseq_rpkm
	23	pn_rpkm
	24	self-pn_l2r
	25	rnaseq-pn_l2r
	26	type


CONTIG_INS_EXON_NOVEL = $(addsuffix .contig.ins.exon.novel,$(SAMPLES))
%.contig.ins.exon.novel: %.contig.ins.exon.log2ratio
	$(call load_modules); \
	cat <<'EOF' | bRscript - | bsort -k 1,1V -k 2,2n >$@
	
	options(width=10000)
	setwd("$(PWD)")
	
	# take header from file
	data.frame = bread(file="$<", header=T)
	
	data.frame$$"self-pn_l2r" = as.numeric( as.character( data.frame$$"self-pn_l2r" ) )

	# select novel deleted functional exons
	novel_funct = data.frame[ which( data.frame$$"self-pn_l2r" >= 1 & data.frame$$"rnaseq_rpkm" > 0 ), ]
	novel_funct$$"type" = rep("functional", nrow(novel_funct))
	
	# select novel deleted non-functional exons
	novel_pseudo = data.frame[ which( data.frame$$"self-pn_l2r" >= 1 & data.frame$$"rnaseq_rpkm" <= 0 ), ]
	novel_pseudo$$"type" = rep("pseudo", nrow(novel_pseudo))
	
	# merge dataframes
	novel = rbind(novel_funct, novel_pseudo)
	
	# sort in increasing order for the "self-pn_l2r" column
	novel = novel[ order(novel$$"self-pn_l2r"), ]
	
	bwrite( novel, row.names=F, col.names=F )
	EOF



novel.ins.exon.report: $(CONTIG_INS_EXON_LOG2RATIO) $(CONTIG_INS_EXON_NOVEL)
	cat >$@ <<EOF
	#sample	novel_functional_insertions	novel_pseudoexon_insertions
	EOF
	
	exon=($(CONTIG_INS_EXON_LOG2RATIO))
	novel_pseudo=($(CONTIG_INS_EXON_NOVEL))
	
	for i in $${!exon[*]}; do
		_tmp=$${exon[$$i]}
		s=$${_tmp%.chr.ins.exon.novel}
		
		n_novel_pseudo="0	0"
		if [ -s $${novel_pseudo[$$i]} ]; then
			n_novel_pseudo=$$(cut -f 26 $${novel_pseudo[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' -e 's/ /\t/g' | select_columns 1 | transpose)
		fi
	
		cat >>$@ <<EOF
		$$s	$$n_novel_pseudo
		EOF
	done

# CONTIG_SELF_PN_COV = $(addsuffix .contig.self.vs.pn.cov.pdf,$(SAMPLES))
# %.contig.self.vs.pn.cov.pdf: %.contig.ins.gene.self.cov %.contig.ins.gene.pn.cov
	# paste <(bawk '!/^[\#,$$]/ { if ( $$feature == "exon" ) { print $$feature_covered } }' $<) \
	# <(bawk '!/^[\#,$$]/ { if ( $$feature == "exon" ) { print $$feature_covered } }' $^2) \
	# | sed '1s/^/self\tpn\n/' \
	# | histogram_plot -r -t histogram -b 20  -o $@ 1 2


# %.exon.25pn.65self: %.contig.ins.gene.self.cov %.contig.ins.gene.pn.cov
	# paste <(sort -k1,1 -k2,2n $<) <(sort -k1,1 -k2,2n $^2) \
	# | bawk '!/^[\#,$$]/ { \
	# if ( $$1 != $$15 || $$4 != $$18 ) { exit 1 } \   * check consistency of the joined input files columns *
	# if ( $$14 >= 0.65 && $$28 <= 0.25 ) { print $$0 } \
	# }' \
	# | cut -f 1-14 >$@



# DELETIONS






.PHONY: test
test:
	@echo $(CHAIN)


ALL += $(CONTIG_INS_EXON_NOVEL) \
	novel.ins.exon.report \
	$(CONTIG_INS_EXON_LOG2RATIO) \
	$(CONTIG_INS_GENE_FAILED) \
	$(CONTIG_INS_GENE_RNASEQ_FAILED) \
	$(CONTIG_INS_GENE_PN_COV) \
	$(CONTIG_INS_GENE_SELF_COV) \
	$(CONTIG_INS_GENE_RNASEQ_COV) \
	$(CONTIG_INS_GENE_TAB)


INTERMEDIATE += 


CLEAN += 
