# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:



# DEMULTIPLEX REPEATS ANNOTATIONS

## INSERTIONS
ins.demultiplex: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_1/reference.fasta.repeat.all
	bawk 'BEGIN { delete arr; } \
	{ split($$1,a,"."); arr[a[1]]++;  } \
	END { for  ( i in arr ) print i, arr[i] }' $< \
	| bsort -k 1,1 >$@

ins.demultiplex.t: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.map /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/
	translate $< 1 <$^2 \
	| bawk 'BEGIN { delete arr; } \
	{ split($$1,a,"."); arr[a[1]]++;  } \
	END { for  ( i in arr ) print i, arr[i] }' \
	| bsort -k 1,1 >$@
	
ins.demultiplex.fasta: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_1/reference.fasta.cleaned
	fasta2tab <$< \
	| bawk 'BEGIN { delete arr; } \
	{ split($$1,a,"."); arr[a[1]]++;  } \
	END { for  ( i in arr ) print i, arr[i] }' \
	| bsort -k 1,1 >$@


### classified annotations
#### demultiplex classified annotation for each sample
INS_CLASSIFIED_DEMUX = $(addsuffix .classified.scaff.ins.demux,$(SAMPLES))
%.classified.scaff.ins.demux: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.classified
	bawk '{ split($$1,a,"."); \
	print $$1, $$5 >a[1]".classified.scaff.ins.demux" }' <$<

classified.scaff.ins.annot: $(INS_CLASSIFIED_DEMUX) /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.classified
	REVERSE_FILE_LIST=$$(echo "$(INS_CLASSIFIED_DEMUX)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')

	cut -f 5 $(call last,$^) \
	| bsort \
	| uniq >$@

	TMP=$$(mktemp -u tmp.XXX)
	for F in $(INS_CLASSIFIED_DEMUX); do\
		TMP=$$(translate -a -v <(cut -f2 <"$$F" | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1 | bsort) 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@


### incomplete annotations
INS_INCOMPLETE_DEMUX = $(addsuffix .incomplete.scaff.ins.demux,$(SAMPLES))
%.incomplete.scaff.ins.demux: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.map /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.norm.solo
	translate $< 1 <$^2 \
	| bawk '{ split($$1,a,"."); \
	print $$1, $$4 >a[1]".incomplete.scaff.ins.demux" }'

incomplete.scaff.ins.annot: $(INS_INCOMPLETE_DEMUX) /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.norm.solo
	REVERSE_FILE_LIST=$$(echo "$(INS_INCOMPLETE_DEMUX)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')

	cut -f 4 $(call last,$^) \
	| bsort \
	| uniq >$@

	TMP=$$(mktemp -u tmp.XXX)
	for F in $(INS_INCOMPLETE_DEMUX); do\
		TMP=$$(translate -a -v <(cut -f2 <"$$F" | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1 | bsort) 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@





### incoherent annotations
INS_INCOHERENT_DEMUX = $(addsuffix .incoherent.scaff.ins.demux,$(SAMPLES))
%.incoherent.scaff.ins.demux: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.map /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.norm.incoherent
	translate $< 1 <$^2 \
	| bawk '{ split($$1,a,"."); \
	print $$1, $$5 >a[1]".incoherent.scaff.ins.demux" }'

incoherent.scaff.ins.annot: $(INS_INCOHERENT_DEMUX) /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(INS_FILTER)/phase_2/reference.norm.incoherent
	REVERSE_FILE_LIST=$$(echo "$(INS_INCOHERENT_DEMUX)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')

	cut -f 5 $(call last,$^) \
	| bsort \
	| uniq >$@

	TMP=$$(mktemp -u tmp.XXX)
	for F in $(INS_INCOHERENT_DEMUX); do\
		TMP=$$(translate -a -v <(cut -f2 <"$$F" | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1 | bsort) 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@






## DELETIONS
### classified annotations
#### demultiplex classified annotation for each sample
DEL_CLASSIFIED_DEMUX = $(addsuffix .classified.scaff.del.demux,$(SAMPLES))
%.classified.scaff.del.demux: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.classified
	bawk '{ split($$1,a,"."); \
	print $$1, $$5 >a[1]".classified.scaff.del.demux" }' <$<

classified.scaff.del.annot: $(DEL_CLASSIFIED_DEMUX) /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.classified
	REVERSE_FILE_LIST=$$(echo "$(DEL_CLASSIFIED_DEMUX)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')

	cut -f 5 $(call last,$^) \
	| bsort \
	| uniq >$@

	TMP=$$(mktemp -u tmp.XXX)
	for F in $(DEL_CLASSIFIED_DEMUX); do\
		TMP=$$(translate -a -v <(cut -f2 <"$$F" | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1 | bsort) 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@


### incomplete annotations
DEL_INCOMPLETE_DEMUX = $(addsuffix .incomplete.scaff.del.demux,$(SAMPLES))
%.incomplete.scaff.del.demux: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.map /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.norm.solo
	translate $< 1 <$^2 \
	| bawk '{ split($$1,a,"."); \
	print $$1, $$4 >a[1]".incomplete.scaff.del.demux" }'

incomplete.scaff.del.annot: $(DEL_INCOMPLETE_DEMUX) /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.norm.solo
	REVERSE_FILE_LIST=$$(echo "$(DEL_INCOMPLETE_DEMUX)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')

	cut -f 4 $(call last,$^) \
	| bsort \
	| uniq >$@

	TMP=$$(mktemp -u tmp.XXX)
	for F in $(DEL_INCOMPLETE_DEMUX); do\
		TMP=$$(translate -a -v <(cut -f2 <"$$F" | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1 | bsort) 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@


### incoherent annotations
DEL_INCOHERENT_DEMUX = $(addsuffix .incoherent.scaff.del.demux,$(SAMPLES))
%.incoherent.scaff.del.demux: /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.map /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.norm.incoherent
	translate $< 1 <$^2 \
	| bawk '{ split($$1,a,"."); \
	print $$1, $$5 >a[1]".incoherent.scaff.del.demux" }'

incoherent.scaff.del.annot: $(DEL_INCOHERENT_DEMUX) /mnt/vol1/projects/novabreed/share/mvidotto/bioinfotree/prj/ltr_find/dataset/asstics_$(DEL_FILTER)/phase_2/reference.norm.incoherent
	REVERSE_FILE_LIST=$$(echo "$(DEL_INCOHERENT_DEMUX)" | awk '{ for (i=NF; i>1; i--) { split($$i,a,"."); printf("%s\t",a[1]); } split($$1,a,"."); print a[1]; }')

	cut -f 5 $(call last,$^) \
	| bsort \
	| uniq >$@

	TMP=$$(mktemp -u tmp.XXX)
	for F in $(DEL_INCOHERENT_DEMUX); do\
		TMP=$$(translate -a -v <(cut -f2 <"$$F" | bsort | uniq -c | sed -e 's/^[ \t]*//' | tr ' ' \\t | select_columns 2 1 | bsort) 1 <$@)
		echo "$$TMP" >$@
	done
	# append to at the beginning of the file
	sed -i '1s/^/\t'"$$REVERSE_FILE_LIST"'\n/' $@

#####


.PHONY: test
test:
	@echo


ALL += 	classified.scaff.ins.annot \
		incomplete.scaff.ins.annot \
		incoherent.scaff.ins.annot \
		\
		classified.scaff.del.annot \
		incomplete.scaff.del.annot \
		incoherent.scaff.del.annot
		

INTERMEDIATE += $(INS_CLASSIFIED_DEMUX) \
				$(INS_INCOMPLETE_DEMUX) \
				$(INS_INCOHERENT_DEMUX) \
				$(DEL_CLASSIFIED_DEMUX) \
				$(DEL_INCOMPLETE_DEMUX) \
				$(DEL_INCOHERENT_DEMUX)

CLEAN += 
