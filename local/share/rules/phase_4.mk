# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

.ONESHELL:

log:
	mkdir -p $@

ref.fa:
	ln -sf $(REF) $@

ref.merged.nodup.bam:
	ln -sf $(PN_DNA_ALN_REF) $@
	
ref.chr.del.gene.rnaseq.cov:
	ln -sf $(PN_RNASEQ_COV_REF) $@




# this function install all the links at once
REALN_BAM = $(addsuffix .self.merged.nodup.bam,$(SAMPLES))
GENE_TAB = $(addsuffix .chr.del.gene.tab,$(SAMPLES))
%.self.merged.nodup.bam %.chr.del.gene.tab:
	@echo installing links ...
	$(foreach SAMPLE,$(SAMPLES), \
		$(shell ln -sf $(call get,$(SAMPLE),GENE_TAB) $(SAMPLE).chr.del.gene.tab) \
		$(shell ln -sf $(call get,$(SAMPLE),SELF_DNA_ALN_REF) $(SAMPLE).self.merged.nodup.bam) \
	) \
	sleep 3


# DELEZIONI

# rnaseq coverage
# select only genes aready selected in %.chr.ins.gene.tab
.META: %.chr.del.gene.rnaseq.cov ref.chr.del.gene.rnaseq.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	overlapping_reads
	12	covered_positions
	13	feature_length
	14	feature_covered


CHR_DEL_GENE_RNASEQ_COV = $(addsuffix .chr.del.gene.rnaseq.cov,$(SAMPLES))
%.chr.del.gene.rnaseq.cov: %.chr.del.gene.tab ref.chr.del.gene.rnaseq.cov
	cut -f 1-10 <$< \   * 10 fields are needed to guarantie uniqness of dictionary *
	| sed -e 's/\t/!/g' \
	| translate -a <(sed -e 's/\t/!/g' -e 's/!/\t/10g' <$^2) 1 \
	| sed -e 's/!/\t/g' >$@

ref.rnaseq.fstats:
	ln -sf $(PN_RNASEQ_FSTATS_REF) $@

# self coverage
.META: %.chr.del.gene.self.cov %.chr.del.gene.ref.cov
	1	reference
	2	start
	3	end
	4	name  
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	reference
	12	ref_start
	13	ref_stop
	14	ID
	15	size
	16	strand
	17	type
	18	ref_gap_size
	19	query_gap_size
	20	query_coordinates
	21	method
	22	overlapping_bases
	23	overlapping_reads
	24	covered_positions
	25	feature_length
	26	feature_covered


CHR_DEL_GENE_SELF_COV = $(addsuffix .chr.del.gene.self.cov,$(SAMPLES))
.PRECIOUS: %.chr.del.gene.self.cov
%.chr.del.gene.self.cov: %.self.merged.nodup.bam %.chr.del.gene.tab
	$(call load_modules); \
	bedtools coverage \
	-abam $< \
	-b $^2 \
	>$@


FSTATS = $(addsuffix .self.fstats,$(SAMPLES))
%.self.fstats: %.self.merged.nodup.bam
	$(call load_modules); \
	samtools flagstat $< \
	| bawk '!/^[\#+,$$]/ { \
	split($$0,a,"+ 0 "); \
	gsub(" ","",a[1]); \
	print a[1]; }' \
	| transpose \
	| sed 's/^/$@\t/' >$@


# pn coverage
CHR_DEL_GENE_REF_COV = $(addsuffix .chr.del.gene.ref.cov,$(SAMPLES))
.PRECIOUS: %.chr.del.gene.ref.cov
%.chr.del.gene.ref.cov: ref.merged.nodup.bam %.chr.del.gene.tab
	$(call load_modules); \
	bedtools coverage \
	-abam $< \
	-b $^2 \
	>$@


.META: ref.rnaseq.fstats
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	duplicates
	4	mapped (98.79%:-nan%)
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired (97.16%:-nan%)
	9	with itself and mate mapped
	10	singletons (0.83%:-nan%)
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)


.META: ref.fstats %.self.fstats
	1	sample
	2	in total (QC-passed reads + QC-failed reads)
	3	secondary
	4	supplementary
	3	duplicates
	4	mapped (100.00% : N/A)
	5	paired in sequencing
	6	read1
	7	read2
	8	properly paired (97.87% : N/A)
	9	with itself and mate mapped
	10	singletons (0.28% : N/A)
	11	with mate mapped to a different chr
	12	with mate mapped to a different chr (mapQ>=5)

ref.fstats: ref.merged.nodup.bam
	$(call load_modules); \
	samtools flagstat $< \
	| bawk '!/^[\#+,$$]/ { \
	split($$0,a,"+ 0 "); \
	gsub(" ","",a[1]); \
	print a[1]; }' \
	| transpose \
	| sed 's/^/$@\t/' >$@





CONTIG_DEL_EXON_LOG2RATIO = $(addsuffix .chr.del.exon.log2ratio,$(SAMPLES))
%.chr.del.exon.log2ratio: %.chr.del.gene.self.cov %.self.fstats %.chr.del.gene.rnaseq.cov ref.rnaseq.fstats %.chr.del.gene.ref.cov ref.fstats
	$(call load_modules); \
	cat <<'EOF' | bRscript -
	
	options(width=10000)
	setwd("$(PWD)")
	
	# standard implementation
	countToTpm = function(counts, mapped, effLen) {
		rpk = counts / effLen
		denom = mapped / 1e6
		return(rpk / denom)
	}
	
	countToFpkm = function(counts, mapped, effLen) {
		return( exp( log(counts) + log(1e9) - log(effLen) - log(mapped) ) )
	}
	
	# define header
	fh = c("reference",	"start", "end", "name", "score", "strand", "attribute", "feature", "frame",
	"description", "reference", "ref_start", "ref_stop", "ID", "size", "strand", "type", "ref_gap_size", "query_gap_size",
	"query_coordinates", "method", "overlapping_bases")

	fh1 = c("reference", "start", "end", "name", "score", "strand",
	"attribute","feature","frame","description")
	
	# self data
	vh = c("self_overlapping_reads","self_covered_positions","self_feature_length","self_feature_covered")
	self_data = bread(file="$<", header=F)
	colnames(self_data) = c(fh,vh)
	# this datataframe contains more columns that the rnaseq dataframe.
	# here I select the same columns as in rnaseq dataframe
	self_data = self_data[,c(fh1,vh)]

	self_fstat = bread(file="$^2", header=F, check.names=T)
	# the number of aligned reads is in position 7 of "self.fstats"
	self_mapped = as.integer(self_fstat[1,6])
	
	self_counts = as.integer(self_data[,11])
	self_len = self_data[,13]/1000
	self_data$$"self_rpkm" = with(self_data, countToFpkm(self_counts, self_mapped, self_len))
	

	# rnaseq data
	vh = c("rnaseq_overlapping_reads","rnaseq_covered_positions","rnaseq_feature_length","rnaseq_feature_covered")
	rnaseq_data = bread(file="$^3", header=F)
	colnames(rnaseq_data) = c(fh1,vh)
	
	rnaseq_fstat = bread(file="$^4", header=F, check.names=T)
	rnaseq_mapped = as.integer(rnaseq_fstat[1,4])
	
	rnaseq_counts = as.integer(rnaseq_data[,11])
	rnaseq_len = rnaseq_data[,13]/1000
	rnaseq_data$$"rnaseq_rpkm" = with(rnaseq_data[,fh1], countToFpkm(rnaseq_counts, rnaseq_mapped, rnaseq_len))
	
	# pn data
	vh = c("pn_overlapping_reads","pn_covered_positions","pn_feature_length","pn_feature_covered")
	pn_data = bread(file="$^5", header=F)
	colnames(pn_data) = c(fh,vh)
	# this datataframe contains more columns that the rnaseq dataframe.
	# here I select the same columns as in rnaseq dataframe
	pn_data = pn_data[,c(fh1,vh)]
	
	pn_fstat = bread(file="$^6", header=F, check.names=T)
	pn_mapped = as.integer(pn_fstat[1,6])
	
	pn_counts = as.integer(pn_data[,11])
	pn_len = pn_data[,13]/1000
	pn_data$$"pn_rpkm" = with(pn_data, countToFpkm(pn_counts, pn_mapped, pn_len))

	
	# join everything
	merged_data = merge(merge(self_data, pn_data, by=fh1, sort=F), rnaseq_data, by=fh1, sort=F)
	
	# calculate log 2 ratio
	merged_data$$"self-pn_l2r" = with(merged_data, log2( (merged_data$$"self_rpkm" + 0.1) / (merged_data$$"pn_rpkm" + 0.1) ))
	merged_data$$"self-rnaseq_l2r" = with(merged_data, log2( (merged_data$$"self_rpkm" + 0.1) / (merged_data$$"rnaseq_rpkm" + 0.1) ))
	
	# select exons
	exondata = merged_data[ which(merged_data[,8] == 'exon'), ]
	
	# reorder columns
	# remove duplicated lines
	exondata = unique( 
					exondata[ c(fh1,  "self_feature_length",
								"self_covered_positions", "rnaseq_covered_positions", "pn_covered_positions",
								"self_overlapping_reads", "rnaseq_overlapping_reads", "pn_overlapping_reads",
								"self_feature_covered", "rnaseq_feature_covered", "pn_feature_covered",
								"self_rpkm", "rnaseq_rpkm", "pn_rpkm",
								"self-pn_l2r", "self-rnaseq_l2r" ) ]
					)
	
	colnames(exondata)[which(names(exondata) == "self_feature_length")] = "feature_length"
	
	# print to file
	bwrite(exondata, file="$@", row.names=F)
	EOF


.META: %.chr.del.exon.novel
	1	reference
	2	start
	3	end
	4	name
	5	score
	6	strand
	7	attribute
	8	feature
	9	frame
	10	description
	11	feature_length
	12	self_covered_positions
	13	rnaseq_covered_positions
	14	pn_covered_positions
	15	self_overlapping_reads
	16	rnaseq_overlapping_reads
	17	pn_overlapping_reads
	18	self_feature_covered
	19	rnaseq_feature_covered
	20	pn_feature_covered
	21	self_rpkm
	22	rnaseq_rpkm
	23	pn_rpkm
	24	self-pn_l2r
	25	self-rnaseq_l2r
	26	type


CONTIG_DEL_EXON_NOVEL = $(addsuffix .chr.del.exon.novel,$(SAMPLES))
%.chr.del.exon.novel: %.chr.del.exon.log2ratio
	$(call load_modules); \
	cat <<'EOF' | bRscript - | bsort -k 1,1V -k 2,2n >$@
	
	options(width=10000)
	setwd("$(PWD)")
	
	# take header from file
	data.frame = bread(file="$<", header=T)
	
	data.frame$$"self-pn_l2r" = as.numeric( as.character( data.frame$$"self-pn_l2r" ) )

	# select novel deleted functional exons
	novel_funct = data.frame[ which( data.frame$$"self-pn_l2r" <= -1 & data.frame$$"rnaseq_rpkm" > 0 ), ]
	novel_funct$$"type" = rep("functional", nrow(novel_funct))
	
	# select novel deleted non-functional exons
	novel_pseudo = data.frame[ which( data.frame$$"self-pn_l2r" <= -1 & data.frame$$"rnaseq_rpkm" <= 0 ), ]
	novel_pseudo$$"type" = rep("pseudo", nrow(novel_pseudo))
	
	# merge dataframes
	novel = rbind(novel_funct, novel_pseudo)
	
	# sort in increasing order for the "self-pn_l2r" column
	novel = novel[ order(novel$$"self-pn_l2r"), ]
	
	bwrite( novel, row.names=F, col.names=F )
	EOF


novel.del.exon.report: $(CONTIG_DEL_EXON_LOG2RATIO) $(CONTIG_DEL_EXON_NOVEL)
	cat >$@ <<EOF
	#sample	novel_functional_deletion	novel_pseudoexon_deletion
	EOF
	
	exon=($(CONTIG_DEL_EXON_LOG2RATIO))
	novel_pseudo=($(CONTIG_DEL_EXON_NOVEL))
	
	for i in $${!exon[*]}; do
		_tmp=$${exon[$$i]}
		s=$${_tmp%.chr.del.exon.novel}
		
		n_novel_pseudo="0	0"
		if [ -s $${novel_pseudo[$$i]} ]; then
			n_novel_pseudo=$$(cut -f 26 $${novel_pseudo[$$i]} | bsort | uniq -c | sed -e 's/^[ \t]*//' -e 's/ /\t/g' | select_columns 1 | transpose)
		fi
	
		cat >>$@ <<EOF
		$$s	$$n_novel_pseudo
		EOF
	done
	


.PHONY: test
test:
	@echo 



ALL += $(CONTIG_DEL_EXON_LOG2RATIO) \
	$(CHR_DEL_GENE_SELF_COV) \
	$(CHR_DEL_GENE_REF_COV) \
	$(CHR_DEL_GENE_RNASEQ_COV) \
	$(CONTIG_DEL_EXON_NOVEL) \
	novel.del.exon.report \
	$(FSTATS) \
	ref.fstats
	


INTERMEDIATE += 


CLEAN += 
